# Введение

**Область применения Java**

- веб-приложения
- мобильные приложения
- серверные приложения
- корпоративные приложения

**Достоинства и недостатки Java**

- популярный (например: [TIOBE](https://www.tiobe.com/tiobe-index/))
- Объектно-ориентированный подход
- Java Virtual Machine
- Garbage collector

**Инструментарий**

- Java Development Kit (обзор компиляторов)
  - Oracle JDK (платный)
  - Oracle OpenJDK (бесплатный)
  - AdoptOpenJDK (бесплатный)
  - Red Hat OpenJDK (поставляется в составе RHEL)
  - Azul Zulu (платный)
  - Amazon Coretto (бесплатный)
  - другие (IBM, SAPMachine, IcedTEA)
- Версии языка
  - 1.0
  - 1.2
  - 1.3.
  - 1.4
  - 1.5
  - 6 (1.7)
  - 7
  - 8
  - 9 (сентябрь 2017 ?)
  - 10 (март 2018 ?)
  - 11 (сентябрь 2018)
  - 12 (март 2019)
  - 13 (сентябрь 2019)
- java -version — версия Java Runtime Engine
- javac -version — версия Java Development Kit
- Среды разработки:
  - Eclipse
  - IntelliJ IDEA Community
  - IntelliJ IDEA Ultimate (платная, либо студенческая, либо GPL-проект)
- Системы сборки — для компиляции больших проектов
  - Apache Ant (устарел)
  - Apache Maven
  - Gradle

**Ввод-вывод на консоль**

**Примитивные типы данных:**

- boolean
- byte - целое 1 байт
- short - целое 2 байт
- int - целое 4 байта
- long - целое 8 байт
- char - 1 символ Unicode 2 байта
- float -  вещественное 4 байта
- double -  вещественное 8 байта

**Все остальные типы данных — это классы.**

Для примитивных типов данных существуют объектно-ориентированные аналоги:

- Boolean
- Byte - целое 1 байт
- Short - целое 2 байт
- Integer - целое 4 байта
- Long - целое 8 байт
- Char - 1 символ Unicode 2 байта
- Float -  вещественное 4 байта
- Double -  вещественное 8 байта

Тип данных: int, Integer, String
Синтаксические конструкции: цикл for, цикл while, функции

